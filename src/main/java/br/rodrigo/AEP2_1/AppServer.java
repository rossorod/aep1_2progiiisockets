package br.rodrigo.AEP2_1;

import java.io.File;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class AppServer {
	public static void main(String[] args) {
		AppServer server = new AppServer();
		server.start();
	}
	
	private void start() {
		try {
			ServerSocket socket = new ServerSocket(12345);
			Socket client = socket.accept();
			Scanner fromClient = new Scanner(client.getInputStream());
			PrintWriter toClient = new PrintWriter(client.getOutputStream());
			String comandoDoClient = "";
			do {
				comandoDoClient = fromClient.nextLine();
				if (!comandoDoClient.equals("sair")) {
					if (comandoDoClient.contains("server")) {
						comandoDoClient = listarArquivos(comandoDoClient);
					}
					toClient.println("ECHO FROM SERVER: [" + comandoDoClient + "]");
					toClient.flush();
				} else {
					toClient.println(comandoDoClient);
					toClient.flush();
				}
			} while (!comandoDoClient.equals("sair"));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String listarArquivos(String pasta) {
		String arquivos = "";
		try {
			File raiz = new File("c:/" + pasta);
			arquivos = percorreRecursivo(raiz, "");
			//File paraRemover = new File("c:/apresentacoes/teste.txt");
			//paraRemover.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivos;
	}
	
	private static String percorreRecursivo(File entrada, String identacao) {
		String arquivos = identacao + entrada.getName();
		if (entrada.isDirectory()) {
			for (File arquivo : entrada.listFiles()) {
				arquivos += percorreRecursivo(arquivo, identacao + " ");
			}
		}
		return arquivos;
	}
}
